from dataclasses import dataclass, field
from ipycanvas import Canvas
from ipywidgets import Image
from typing import List
from Coordinates import Coordinates

@dataclass
class Sprite:
    coordinates: Coordinates
    pixel_width: int
    pixel_height: int
    frames: List[Canvas] = field(default_factory=list)
    current_frame: int = 0

    def add_frame_from_file(self, filename: str, frame_width: int = None, frame_height: int = None) -> None:
        """Adds a frame from a file."""
        if frame_width is None: frame_width = self.pixel_width
        if frame_height is None: frame_height = self.pixel_height
        
        temp_canvas = Canvas(width=frame_width, height=frame_height)
        temp_canvas.draw_image(Image.from_file(filename), 0, 0, frame_width, frame_height)
        
        self.frames.append(temp_canvas)

    def draw(self, canvas: Canvas, frame: int = None) -> None:
        
        if frame is None: frame = self.current_frame
        
        # iterate to next frame
        self.current_frame = (self.current_frame + 1) % len(self.frames)

        canvas.draw_image(
            self.frames[frame],
            self.coordinates.pixel_x_y()[0] - self.pixel_width / 2,
            self.coordinates.pixel_x_y()[1] - self.pixel_height / 2,
            self.pixel_width,
            self.pixel_height)    