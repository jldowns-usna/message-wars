from dataclasses import dataclass
from Direction import Direction
import copy


#######################
## Coordinates
#######################
@dataclass
class Coordinates:
    """To keep track of pixel and grid coordinates.
    """
    grid_x: float
    grid_y: float
    grid_size: float = 50.0
    padding_x: float = 0.0
    padding_y: float = 0.0

    def pixel_x_y(self) -> (float, float):
        """Returns the pixel coordinates."""
        return (self.grid_x * self.grid_size + self.padding_x,
                self.grid_y * self.grid_size + self.padding_y)
    
    def grid_x_y(self) -> (float, float):
        """Returns the grid coordinates."""
        return (self.grid_x, self.grid_y)
    
    def one_north(self) -> None:
        """Returns a copy of the coordinates one grid unit north."""
        c = copy.copy(self)
        c.grid_y -= 1
        return c
    
    def one_south(self) -> None:
        """Returns a copy of the coordinates one grid unit south."""
        c = copy.copy(self)
        c.grid_y += 1
        return c
    
    def one_east(self) -> None:
        """Returns a copy of the coordinates one grid unit east."""
        c = copy.copy(self)
        c.grid_x += 1
        return c
    
    def one_west(self) -> None:
        """Returns a copy of the coordinates one grid unit west."""
        c = copy.copy(self)
        c.grid_x -= 1
        return c
    
    def one_in_direction(self, direction: Direction) -> None:
        """Returns a copy of the coordinates one grid unit in the given direction."""
        c = copy.copy(self)
        if direction == Direction.NORTH:
            c.grid_y -= 1
        elif direction == Direction.SOUTH:
            c.grid_y += 1
        elif direction == Direction.EAST:
            c.grid_x += 1
        elif direction == Direction.WEST:
            c.grid_x -= 1
        return c
    
    def interpolate(origin: "Coordinates", destination: "Coordinates", percent: float) -> "Coordinates":
        """Returns the coordinates that is the given percentage between the origin and destination.
        
        Args:
            origin (Coordinates): The origin coordinates.
            destination (Coordinates): The destination coordinates.
            percent (float): The percentage (0.0 - 1.0) between the origin and destination.
        """
        c = copy.copy(origin)
        c.grid_x = origin.grid_x + (destination.grid_x - origin.grid_x) * percent
        c.grid_y = origin.grid_y + (destination.grid_y - origin.grid_y) * percent
        return c
        