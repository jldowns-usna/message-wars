from dataclasses import dataclass
from typing import List, Optional, Callable
from Entities import Direction
from World import Coordinates, World
from ipycanvas import Canvas, hold_canvas
import random
import time
import logging
logging.basicConfig(level=logging.DEBUG)


from Entities import *
from World import *

class UserNode(Node):
    """A node with a custom on_receive method."""

    def __init__(
        self,
        world: World,
        id: str,
        coordinates: Coordinates,
        on_receive: Callable[[Node, str, str], None],
        direction: Direction = Direction.NONE,
        ):
        """Initializes the node.
        
        Args:
            world: The world in which the node exists.
            id: The node's id.
            coordinates: The node's coordinates.
            on_receive (Callable[[str, str], None]): A function that is called when a message is received.
                The function takes two arguments: the message (str) and the interface (str).
            direction: The node's direction. Defaults to Direction.NONE."""
        super().__init__(world, id, coordinates, direction)

        # The on_receive method is called when a message is received
        self.on_receive = on_receive



class Scenario:

    def __init__(self, node_on_receive:callable) -> None:
        # Create a world with two entities connected by a wire
        self.world = World([
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None]])

        self.world.add_entity(
            UserNode(
                id="A",
                coordinates= Coordinates(grid_x=0, grid_y=3),
                world=self.world,
                on_receive=node_on_receive
                ))
        self.world.add_entity(
            UserNode(
                id="B",
                coordinates= Coordinates(grid_x=4, grid_y=3),
                world=self.world,
                on_receive=node_on_receive
                ))
        self.world.add_tile(
            EW_Wire(
                coordinates= Coordinates(grid_x=1, grid_y=3),
                world=self.world
                ))
        self.world.add_tile(
            EW_Wire(
                coordinates= Coordinates(grid_x=2, grid_y=3),
                world=self.world
                ))
        self.world.add_tile(
            EW_Wire(
                coordinates= Coordinates(grid_x=3, grid_y=3),
                world=self.world
                ))


    def run(self) -> None:
        canvas = Canvas(width=300, height=300)
        display(canvas)

        self.world.update_grid_settings(grid_size=50, padding_x=50, padding_y=50)
        self.world.draw(canvas)

        self.world.entities[0].send_message("Hello, world!", "E")


        while True:
            # Update the world on tick
            self.world.update()
            
            # Animate the update (use 10 frames)
            for i in range(50):
                with hold_canvas(canvas):
                    self.world.move(i/50)
                    canvas.clear()
                    self.world.draw(canvas)
                    time.sleep(0.05)
            
            self.world.post_move()

