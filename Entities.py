from dataclasses import dataclass
from World import *
from Coordinates import Coordinates
from Direction import Direction
from World import Coordinates, Direction, World
from ipycanvas import Canvas
import random
import logging

class Entity:
    """A superclass for dynamic entities."""
    def __init__(
            self,
            world      : World,
            id         : str,
            coordinates: Coordinates,
            direction  : Direction = Direction.NONE,
            ): 
        self.id = id
        self.coordinates = coordinates
        self.destination = coordinates
        self.direction = direction
        self.world = world
        self.sprite = None

    def move(self, percent: float) -> None:
        """Move the entity toward its destination by the given percentage."""
        self.coordinates = Coordinates.interpolate(self.coordinates, self.destination, percent)
        if self.sprite:
            self.sprite.coordinates = self.coordinates

    def post_move(self) -> None:
        """Called after the entity moves."""
        self.coordinates = self.destination
        if self.sprite:
            self.sprite.coordinates = self.coordinates

class Node(Entity):
    """A node that can send and receive messages"""
    def __str__(self) -> str:
        return self.id
    
    def __init__(
        self,
        world: World,
        id: str,
        coordinates: Coordinates,
        direction: Direction = Direction.NONE
        ):
        super().__init__(world, id, coordinates, direction)
        grid_size = self.coordinates.grid_size
        self.sprite = Sprite(self.coordinates, grid_size, grid_size)
        self.sprite.add_frame_from_file(
            filename="node-city.png",
            frame_width=grid_size,
            frame_height=grid_size)

    def draw(self, canvas):
        self.sprite.draw(canvas)


    def on_receive(self, message: str, interface: str) -> None:
        """Called when a message is received"""
        raise NotImplementedError("Nodes must implement on_receive")
    
    def update(self) -> None:
        """Update node state based on current conditions.
        For moving nodes, `update` will set the destination but not actually move the node.
        """
        pass
    
    def send_message(
            self,
            message: str,
            interface: str,
            ) -> None:
        """Sends a message on an interface.
        
        Args:
            message (str): The message to send.
            interface (str): The interface to send the message on. Good values: "N", "S", "E", "W".
        """
        
        # Create a Message on a tile one grid unit away in the direction of the interface.
        # The message will move in that direction.

        # figure out message coordinates
        if interface == "N":
            msg_coordinates = self.coordinates.one_north()
        elif interface == "S":
            msg_coordinates = self.coordinates.one_south()
        elif interface == "E":
            msg_coordinates = self.coordinates.one_east()
        elif interface == "W":
            msg_coordinates = self.coordinates.one_west()
        else:
            raise ValueError(f"Invalid interface: {interface}")

        msg = Message(
            world = self.world,
            message = message,
            id = f"{self.id}-{interface}-{random.randint(0, 100000)}",
            coordinates = msg_coordinates,
            direction = Direction.from_interface_name(interface)
            )
        
        logging.debug(f"Sending message {msg.id} at {msg.coordinates} heading {msg.direction}")
        
        # Add the message to the world
        self.world.add_entity(msg)
    
class Message(Entity):
    def __init__(
            self,
            world: World,
            id: str,
            coordinates: Coordinates,
            message: str,
            direction: Direction = Direction.NONE):
        super().__init__(world=world, id=id, coordinates=coordinates, direction=direction)

        self.message = message
        grid_size = self.coordinates.grid_size
        self.sprite = Sprite(self.coordinates, grid_size, grid_size)
        self.sprite.add_frame_from_file(
            filename="train-sprite.png",
            frame_width=grid_size,
            frame_height=grid_size)
        
        logging.debug(f"Bleep bloop! Creating message {id} at {coordinates} heading {direction}")
        

    def draw(self, canvas):
        self.sprite.draw(canvas)

    def update(self) -> None:
        """Set the destination to one grid unit in the direction of the message."""

        
        # If the message has arrived at a node, call the node's on_receive method and remove the message.
        for entity in self.world.entities:
            if isinstance(entity, Node) and entity.coordinates == self.coordinates:

                # Find the interface name the message arrived on. This will be the opposite of the message's direction.
                # For example, if the message is moving north, it arrived on the "S" interface.
                interface = "Error"
                if self.direction == Direction.NORTH:
                    interface = "S"
                elif self.direction == Direction.SOUTH:
                    interface = "N"
                elif self.direction == Direction.EAST:
                    interface = "W"
                elif self.direction == Direction.WEST:
                    interface = "E"

                logging.debug(f"Message {self.id} arrived at node {entity.id}")
                entity.on_receive(entity, self.message, interface)
                self.world.remove_entity(self.id)
                return
        
        # Move the message (and the sprite?)
        self.destination = self.coordinates.one_in_direction(self.direction)
        self.sprite.coordinates = self.coordinates