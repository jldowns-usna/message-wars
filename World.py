from dataclasses import dataclass
from typing import List
import copy
import logging

from ipycanvas import Canvas, hold_canvas
from Entities import *
from World import *
from Direction import Direction
from Graphics import Sprite
from Coordinates import Coordinates


#######################
## Tiles
#######################

@dataclass
class Tile:
    """A superclass for tile objects that make up the world"""
    coordinates: Coordinates
    world: "World"

    def draw(self, canvas):
        canvas.fill_style = "red"
        canvas.fill_circle(*self.coordinates.pixel_x_y(), 7)

    def __str__(self) -> str:
        raise NotImplementedError("Tiles must implement __str__")
    
class Wire(Tile):
    """Messages can move along wires"""
    pass

@dataclass
class EW_Wire(Wire):
    """A wire that runs east-west"""

    def __post_init__(self):
        grid_size = self.coordinates.grid_size
        self.sprite = Sprite(self.coordinates, grid_size, grid_size)
        self.sprite.add_frame_from_file(
            filename="wire-ew.png",
            frame_width=grid_size,
            frame_height=grid_size)

    def draw(self, canvas):
        self.sprite.draw(canvas)

class NS_Wire(Wire):
    """A wire that runs north-south"""
    def __str__(self) -> str:
        return "║"
    
class Four_Way_Wire(Wire):
    """A wire that runs in all directions"""
    def __str__(self) -> str:
        return "╬"


#######################
## World
#######################
class World:

    def __init__(self,
                 tiles: List[List[Tile]] = None,
                 ) -> None:
        self.entities: List[Entity] = []
        self.tiles: List[List[Tile]] = tiles or []

    #
    # Entity management
    #
    def add_entity(self, entity) -> None:
        logging.debug(f"Adding entity {entity.id} at {entity.coordinates}")
        self.entities.append(entity)

    def get_entity_at(self, x, y):
        for entity in self.entities:
            if entity.grid_x == x and entity.grid_y == y:
                return entity
        return None
    
    def remove_entity(self, entity_id: str) -> None:
        """Removes the entity with the given id from the world."""
        for entity in self.entities:
            if entity.id == entity_id:
                self.entities.remove(entity)
                return
        raise ValueError(f"Entity with id {entity_id} not found in world.")

    #
    # Tile management
    #
    def add_tile(self, tile) -> None:
        grid_x, grid_y = tile.coordinates.grid_x_y()
        self.tiles[grid_x][grid_y] = tile

    def get_tile_at(self, x, y):
        return self.tiles[x][y]
    
    #
    # Display
    #

    def update_grid_settings(self, grid_size: float, padding_x: float, padding_y: float):
        """Updates the grid settings for all coordinates"""
        for entity in self.entities:
            entity.coordinates.grid_size = grid_size
            entity.coordinates.padding_x = padding_x
            entity.coordinates.padding_y = padding_y
        for row in self.tiles:
            for tile in row:
                if tile:
                    tile.coordinates.grid_size = grid_size
                    tile.coordinates.padding_x = padding_x
                    tile.coordinates.padding_y = padding_y

    def draw(self, canvas):
        """Draws the world on the canvas"""
        
        for row in self.tiles:
            for tile in row:
                if tile:
                    tile.draw(canvas)

        for entity in self.entities:
            entity.draw(canvas)

    def update(self):
        """Update all Nodes. """
        for entity in self.entities:
            entity.update()
    
    def move(self, percent: float):
        """Animate nodes."""
        for entity in self.entities:
            entity.move(percent)

    def post_move(self):
        """Called after all entities have moved, to give them a chance to clean up."""
        for entity in self.entities:
            entity.post_move()

    def __str__(self) -> str:
        """Prints the world as a grid of characters"""
        rows = []
        
        # Iterate through each row and column in the tiles 2D list
        for y in range(len(self.tiles[0])):
            row = []
            for x in range(len(self.tiles)):
                tile = self.get_tile_at(x, y)
                entity = self.get_entity_at(x, y)

                # Prioritize entity representation over tile representation
                if entity:
                    row.append(str(entity))
                elif tile:
                    row.append(str(tile))
                else:
                    row.append(" ")  # Default to a space if no tile or entity
            rows.append("".join(row))

        # Join rows together with newlines to create the grid representation
        return "\n".join(rows)
