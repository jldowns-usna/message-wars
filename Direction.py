class Direction:
    """An enum class for representing directions"""
    NORTH = "NORTH"
    SOUTH = "SOUTH"
    EAST = "EAST"
    WEST = "WEST"
    NONE = "NONE"

    def from_interface_name(interface: str) -> "Direction":
        """Returns the direction corresponding to the single-letter interface name."""
        if interface == "N":
            return Direction.NORTH
        elif interface == "S":
            return Direction.SOUTH
        elif interface == "E":
            return Direction.EAST
        elif interface == "W":
            return Direction.WEST
        else:
            raise ValueError(f"Invalid interface: {interface}")

